import React from 'react';
import { Link } from 'react-router-dom';

const NotFound = () => (
    <div>
        <h1>Page not found.</h1>

        <img src="https://image.freepik.com/free-vector/error-404-concept-landing-page_52683-18367.jpg" alt="Pic not found." width="600px"></img><br/>

        <Link to="/login" >Return to the login page.</Link>

    </div>
);

export default NotFound;