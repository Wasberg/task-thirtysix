import React from 'react';
import RegisterForm from '../forms/RegisterForm';
import { Link, useHistory } from 'react-router-dom';

const Register = () => {

    const history = useHistory();

    const handleRegisterComplete = (result) => {
        console.log('Triggered from RegisterForm');
        if (result) {
            history.replace("/dashboard");
        }
    };

    return (
        <div>
            <h1>Register your account.</h1>
            <RegisterForm complete={ handleRegisterComplete } />

            <Link to="/login" >Already have an account?</Link>

        </div>
    );
}

export default Register;