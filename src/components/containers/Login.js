import React from 'react';
import LoginForm from '../forms/LoginForm';
import { Link } from 'react-router-dom';

const Login = () => (
    <div>
        <h1>Login to Survey Puppy.</h1>
        <LoginForm/>

        <Link to="/register" >No account? Create one here.</Link>

    </div>
);

export default Login;