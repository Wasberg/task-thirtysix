
import React from 'react';
import DashboardMessage from '../messages/DashboardMessage';

const Dashboard = () => (
    <div>
        <h1>Welcome to the Dashboard.</h1>
        <DashboardMessage/>
    </div>
);

export default Dashboard;